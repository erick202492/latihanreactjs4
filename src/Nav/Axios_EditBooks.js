import React from "react";
import Axios from "axios";
import { Formik, Form, Field, ErrorMessage } from "formik";
import moment from 'moment';

export default class BookList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { book: [] };
  }


  componentDidMount() {
    //this.timerID = setInterval(() => this.updateClock(), 1000);
    Axios.get(
      `http://localhost:4000/books/${this.props.match.params.param}`
    ).then(res => {
      const book = res.data;
      this.setState({ book: book.data });
      // console.log(book);
      // console.log(this.state.book.language);
      return book
    });
  }


  
  // componentWillMount(){
  //     clearInterval(this.timerID)
  // }

  // updateClock()
  // {
  //     this.setState({
  //         date: new Date()
  //     })
  // }
 

  render() {
    return (
      <div>
        {
        console.log("ini adalah inputan "+this.state.book.language)      
        }
        <Formik       
          initialValues={{
            id: "",
            title : "",
            author: "",
            published_date: "",
            pages: "",
            language: "",
            publisher_id: ""
          }}
          validate={values => {
            // const errors = {};
            // if (
            //   !values.title 
            //   ||!values.author
            //   ||!values.published_date
            //   ||!values.pages
            //   ||!values.language
            //   ||!values.publisher_id
            //   ) {
            //   errors.title = "Must be Edit one Field!!";
            // }
            // return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            setTimeout(() => {
              values.id = this.state.book.id
              //values.published_date = moment(values.published_date).format('YYYY-MM-DD')
              

              
              if(values.title === "")
              {
                values.title = this.state.book.title
              }

              if(values.author === "" 
              //&& values.publisher_date === "" 
              // && values.pages === "" 
              // && values.language === "" 
              // && values.publisher_id === ""
              )
              {
                 values.author = this.state.book.author
                 //values.published_date = this.state.book.published_date
              //    values.pages = this.state.book.pages
              //    values.language = this.state.book.language
              //    values.publisher_id = this.state.book.publisher_id
              }
              if(values.published_date === "")
              {
                values.published_date = this.state.book.published_date
                values.published_date = moment(values.published_date).format('YYYY-MM-DD')
              }
              if(values.pages === "")
              {
                values.pages = this.state.book.pages
              }
              if(values.language === "")
              {
                values.language = this.state.book.language
              }
              if(values.publisher_id === "")
              {
                values.publisher_id = this.state.book.publisher_id
              }

              console.log("ini adalah DB id :  "+this.state.book.id)
              console.log("ini adalah DB author : "+this.state.book.title)
              console.log("ini adalah DB author : "+this.state.book.author)
              console.log("ini adalah DB publisher Date "+this.state.book.published_date)
              console.log("ini adalah DB pages : "+this.state.book.pages)
              console.log("ini adalah DB language : "+this.state.book.language)
              console.log("ini adalah DB publisher : "+this.state.book.publisher_id)
              
              console.log("ini adalah id #"+values.id);
              console.log("ini adalah author #"+values.title);
              console.log("ini adalah author #"+values.author);
              console.log("ini adalah publisher_date #"+values.published_date);
              console.log("ini adalah pages #"+values.pages);
              console.log("ini adalah language #"+values.language);
              console.log("ini adalah publisherID #"+values.publisher_id);
              // alert("ini adalah DB "+this.state.book.id)
              
              
              Axios.put(`http://localhost:4000/books/${this.props.match.params.param}`, values)
                .then(response => {
                  alert("Res : " + response.data.message);
                  window.location = "/axios/viewbook"
                })
                .catch(function(error) {
                  console.log("cacth error" + error);
                });

              setSubmitting(false);
            }, 400);
          }}
        >


          {({ isSubmitting, handleChange, dirty }) => (
            <div className="container">
             
            {}
              <Form>
                <div className="form-group row">
                  <div className="input-group-prepend"></div>
                 id
                  <Field value={this.state.book.id} type="text"
                    name="id"                    
                    className="form-control form-control-sm"
                  />
                 Title <Field
                    placeholder={this.state.book.title}
                    type="text"
                    name="title"
                    className="form-control form-control-sm"
                    
                  />
                  Author<Field
                    placeholder={this.state.book.author}
                    type="text"
                    name="author"
                    className="form-control form-control-sm"
                  />
                  Publisher Date : {moment(this.state.book.published_date).format('YYYY-MM-DD')}
                  <Field
                    placeholder='2012/10/10'
                    // placeholder={this.state.book.published_date}
                    type="date"
                    name="published_date"
                    className="form-control form-control-sm"
                  />
                  Pages<Field
                    placeholder={this.state.book.pages}
                    type="number"
                    name="pages"
                    className="form-control form-control-sm"
                  />
                  Language<Field
                    placeholder={this.state.book.language}
                    type=""
                    name="language"
                    className="form-control form-control-sm"
                  />
                  Publisher ID<Field
                    placeholder={this.state.book.publisher_id}
                    type=""
                    name="publisher_id"
                    className="form-control form-control-sm"
                  />
                  <br />
                  <button
                    type="submit"
                    className="btn btn-primary"
                    disabled={!dirty || isSubmitting}
                  >
                    Submit
                  </button>
                  <ErrorMessage
                    name="title"
                    component="div"
                    className="alert alert-danger"
                    role="alert"
                  />
                  <ErrorMessage
                    name="pages"
                    component="div"
                    className="alert alert-danger"
                    role="alert"
                  />
                </div>
              </Form>
            </div>
          )}
        </Formik>
      </div>
    );
  }
}
