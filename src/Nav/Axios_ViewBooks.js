import React from "react";
import Axios from "axios";

export default class BookList extends React.Component {
  //constructor(props)
  state = {
    //   super(props);
    book: []
    
  };

  componentDidMount() {
    //this.timerID = setInterval(() => this.updateClock(), 1000);
    Axios.get("http://localhost:4000/books/").then(res => {
      const book = res.data;
      this.setState({ book : book.data });
      console.log(book)
    });
  }

  // componentWillMount(){
  //     clearInterval(this.timerID)
  // }

  // updateClock()
  // {
  //     this.setState({
  //         date: new Date()
  //     })
  // }

  render() {
    return (
      <div className="container">
        
        <table className="table table-striped">
            <thead>
          <tr>
            <th>id</th>
            <th>title</th>
            <th>author</th>
            <th>publish date</th>
            <th>pages</th>
            <th>language</th>
            <th>publisher id</th>
            <th>Button Edit</th>
            <th>Button Del</th>
            
          </tr>
          </thead>
          <tbody>
          {this.state.book.map(books => (
            <tr key={books.id}>
            <td>{books.id}</td>
              <td>{books.title}</td>
              <td>{books.author}</td>
              <td>{books.published_date}</td>          
              <td>{books.pages}</td>
              <td>{books.language}</td>
              <td>{books.publisher_id}</td>
              <td><a href={"http://localhost:3000/axios/editbook/"+books.id}>Edit</a></td>
          <td><a href={"http://localhost:3000/axios/deletebook/"+books.id}>Delete</a></td>

            </tr>       
          ))}
          </tbody>
        </table>
      </div>
    );
  }
}
