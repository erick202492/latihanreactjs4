// Render Prop
import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import "bootstrap/dist/css/bootstrap.min.css";

const Login = () => (
  <div>
     <h2 align="center"> Login </h2>
    <Formik
      initialValues={{ email: "", password: "",}}
      validate={values => {
        const errors = {};
        if (!values.email) {
          errors.email = "Email Required";
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = "Invalid email address";
        }else if (!values.password) {
          errors.password = "Password Required";
        }
        return errors;
      }}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 400);
      }}
    >
      {({ isSubmitting }) => (
        <div className="container">
        <Form>
          <div class="form-group " >
            <div class="input-group-prepend">
            </div>
            <Field
              placeholder="Email"
              type="email"
              name="email"
              class="form-control"
            />{" "}
            <br />
            <Field
              placeholder="Password"
              type="password"
              name="password"
              class="form-control"
            />
            <br />
            <button
              type="submit"
              class="btn btn-primary"
              disabled={isSubmitting}
            >
              Submit
            </button>
            <ErrorMessage
              name="email"
              component="div"
              class="alert alert-danger"
              role="alert"
            />
            <ErrorMessage
              name="password"
              component="div"
              class="alert alert-danger"
              role="alert"
            />
          
          </div> 
          
                  
        </Form>
        
        </div>
      )}
    </Formik>
  </div>
);

export default Login;
